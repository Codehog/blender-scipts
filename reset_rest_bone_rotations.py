import bpy
from mathutils import Matrix, Vector

for obj in bpy.context.selected_objects:
    if obj.type == "ARMATURE":
        for bone in obj.data.edit_bones:
            bone.tail = bone.head + Vector((0,0,0.1))