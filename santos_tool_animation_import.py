import struct
from mathutils import Matrix, Vector
import bpy
from bpy_extras.io_utils import ImportHelper
from bpy.props import StringProperty, BoolProperty
from bpy.types import Operator
import bmesh

def console_print(*args, **kwargs):
    for a in bpy.context.screen.areas:
        if a.type == 'CONSOLE':
            c = {}
            c['area'] = a
            c['space_data'] = a.spaces.active
            c['region'] = a.regions[-1]
            c['window'] = bpy.context.window
            c['screen'] = bpy.context.screen
            s = " ".join([str(arg) for arg in args])
            for line in s.split("\n"):
                bpy.ops.console.scrollback_append(c, text=line)

dataPath = "F:\Games\steamapps\common\Dawn of War II - Retribution\GameAssets\Data"

def readStr(file, count):
    return file.read(count).decode('ASCII')

def readUByte(file):
    return struct.unpack('B', file.read(1))[0]

def readULong(file):
    return struct.unpack('I', file.read(4))[0]

def readFloat(file):
    return struct.unpack('f', file.read(4))[0]

def readMatrix(file):
    return Matrix([
        [readFloat(file), readFloat(file), readFloat(file), 0],
        [readFloat(file), readFloat(file), readFloat(file), 0],
        [readFloat(file), readFloat(file), readFloat(file), 0],
        [readFloat(file), readFloat(file), readFloat(file), 1],
    ]).transposed()

def importAnimation(file, context, name, armature):
    # load header
    magic = readStr(file, 3)
    if magic != "STA":
        raise Exception("Wrong magic number: file is not an Santos Tool Animation!")
        
    version = readUByte(file)
    console_print("Importing Santos Tool Animation version: ", version)
    
    # load skeleton
    numBones = readULong(file)
    boneNames = []
    bonePoses = {}
    
    for i in range(numBones):
        boneName = readStr(file, readULong(file)).lower()
        matrix = readMatrix(file)
        boneNames.append(boneName)
        bonePoses[boneName] = matrix
        
#        bone = armature.pose.bones.get(boneName)
#        if bone:
#            found = bone.matrix
#            if bone.parent:
#                found = bone.parent.matrix.inverted() @ found
#            console_print("for bone ", boneName, "\nexpected: ", matrix, "\nmatrix: ",  found)

    # load animation
    numFrames = readULong(file)
    frames = []
    for frameId in range(numFrames):
        keys = {}
        for boneId in range(numBones):
            keys[boneNames[boneId]] = readMatrix(file)
        frames.append(keys)      
    
    # create action
    console_print("creating action: ", name)
    armature.animation_data_create()
    armature.animation_data.action = bpy.data.actions.new(name)
     
    # create f curves
    for bone in boneNames:
        if not bone in armature.pose.bones:
            console_print("having bone in animation but not in armature: ", bone)
            continue
        
           
        locationX = armature.animation_data.action.fcurves.new('pose.bones["%s"].location' % bone, index=0)
        locationY = armature.animation_data.action.fcurves.new('pose.bones["%s"].location' % bone, index=1)
        locationZ = armature.animation_data.action.fcurves.new('pose.bones["%s"].location' % bone, index=2)
        rotationW = armature.animation_data.action.fcurves.new('pose.bones["%s"].rotation_quaternion' % bone, index=0)
        rotationX = armature.animation_data.action.fcurves.new('pose.bones["%s"].rotation_quaternion' % bone, index=1)
        rotationY = armature.animation_data.action.fcurves.new('pose.bones["%s"].rotation_quaternion' % bone, index=2)
        rotationZ = armature.animation_data.action.fcurves.new('pose.bones["%s"].rotation_quaternion' % bone, index=3)
        scaleX = armature.animation_data.action.fcurves.new('pose.bones["%s"].scale' % bone, index=0)
        scaleY = armature.animation_data.action.fcurves.new('pose.bones["%s"].scale' % bone, index=1)
        scaleZ = armature.animation_data.action.fcurves.new('pose.bones["%s"].scale' % bone, index=2)
        
         
        for frameId in range(numFrames):
            mat = frames[frameId][bone]
            
            parent = armature.pose.bones.get(bone).parent
            if parent:
                parent_mat = frames[frameId].get(parent.name)
                if parent_mat:
                    mat = mat @ parent_mat.inverted()
            
            mat = mat @ armature.data.bones[bone].matrix_local
            
            
        
            pos,rot,scale = mat.decompose()
            locationX.keyframe_points.insert(frameId, pos.x, options={'NEEDED'})
            locationY.keyframe_points.insert(frameId, pos.y, options={'NEEDED'})
            locationZ.keyframe_points.insert(frameId, pos.z, options={'NEEDED'})
            rotationW.keyframe_points.insert(frameId, rot.w, options={'NEEDED'})
            rotationX.keyframe_points.insert(frameId, rot.x, options={'NEEDED'})
            rotationY.keyframe_points.insert(frameId, rot.y, options={'NEEDED'})
            rotationZ.keyframe_points.insert(frameId, rot.z, options={'NEEDED'})
            scaleX.keyframe_points.insert(frameId, scale.x, options={'NEEDED'})
            scaleY.keyframe_points.insert(frameId, scale.y, options={'NEEDED'})
            scaleZ.keyframe_points.insert(frameId, scale.z, options={'NEEDED'})
            
            

class ImportSTAData(Operator, ImportHelper):
    """This appears in the tooltip of the operator and in the generated docs"""
    bl_idname = "santos_tool_import.santos_animation_data"  # important since its how bpy.ops.import_test.some_data is constructed
    bl_label = "Animation Import"
    bl_options = {'UNDO'}

    # ImportHelper mixin class uses this
    filename_ext = ".anim"

    filter_glob: StringProperty(
        default="*.anim",
        options={'HIDDEN'},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )

   
    def execute(self, context):
        console_print("running santos tool animation import.")
        f = open(self.filepath, 'rb')

        animationName = self.filepath.split("\\")[-1].split(".")[0]
        for obj in bpy.context.selected_objects:
            if obj.type == "ARMATURE":
                importAnimation(f, context, animationName, obj)

        f.close()

        return {'FINISHED'}


# Only needed if you want to add into a dynamic menu
def menu_func_import(self, context):
    self.layout.operator(ImportSTAData.bl_idname, text="Santos Tool Animation Import")


def register():
    bpy.utils.register_class(ImportSTAData)
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)


def unregister():
#    bpy.utils.unregister_class(ImportSTAData)
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)


if __name__ == "__main__":
    unregister()
    register()