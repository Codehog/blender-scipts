import struct
from math import ceil
from mathutils import Matrix, Vector
import bpy
from bpy_extras.io_utils import ImportHelper
from bpy.props import StringProperty, BoolProperty, CollectionProperty
from bpy.types import Operator
import bmesh
import os

def console_print(*args, **kwargs):
    for a in bpy.context.screen.areas:
        if a.type == 'CONSOLE':
            c = {}
            c['area'] = a
            c['space_data'] = a.spaces.active
            c['region'] = a.regions[-1]
            c['window'] = bpy.context.window
            c['screen'] = bpy.context.screen
            s = " ".join([str(arg) for arg in args])
            for line in s.split("\n"):
                bpy.ops.console.scrollback_append(c, text=line)

dataPath = "F:\Games\steamapps\common\Dawn of War II - Retribution\GameAssets\Data"

class Color():
    def __init__(self, r = 0, g = 0, b = 0, a = 0):
        self.r = r
        self.g = g
        self.b = b
        self.a = a

class VertexElement():
    def __init__(self, _type, version, data_type):
        self.vert_type = type
        self.version = version
        self.data_type = data_type

class BoneData():
    def __init__(self, name, parent, matrix, transform):
        self.name = name
        self.parent = parent
        self.matrix = matrix
        self.transform = transform
        self.numChildren = 0
        

class ChunkStruct():
    def __init__(self, data_type, data_id, version, size, name, unknown1, unknown2, parent, children, offset):
        self.data_type = data_type
        self.data_id = data_id
        self.version = version
        self.size = size
        self.name = name
        self.unknown1 = unknown1
        self.unknown2 = unknown2
        self.parent = parent
        self.children = children
        self.offset = offset

def readStr(file, count):
    return file.read(count).decode('ASCII')

def readLong(file):
    return struct.unpack('i', file.read(4))[0]

def readULong(file):
    return struct.unpack('I', file.read(4))[0]

def readFloat(file):
    return struct.unpack('f', file.read(4))[0]

def readShort(file):
    return struct.unpack('h', file.read(2))[0]

def readUShort(file):
    return struct.unpack('H', file.read(2))[0]

def readByte(file):
    return struct.unpack('b', file.read(1))[0]

def readUByte(file):
    return struct.unpack('B', file.read(1))[0]

def readMatrix(file):
    return Matrix([
        [readFloat(file), readFloat(file), readFloat(file), 0.0],
        [readFloat(file), readFloat(file), readFloat(file), 0.0],
        [readFloat(file), readFloat(file), readFloat(file), 0.0],
        [readFloat(file), readFloat(file), readFloat(file), 1.0],
    ]).transposed()

def get_bit(byteval,idx):
    return ((byteval&(1<<idx))!=0);

def parseChunkyFile(file):
    root = ChunkStruct("ROOT", "ROOT", 1, 0, "", 0, 0, None, [], 0)
    # skip file header
    if readStr(file, 14) != "Relic Chunky\r\n":
        raise Exception("Wrong magic number!")
        
    file.seek(36, 0)
    
    # read chunks recursivly
    readChunk(file, root)
    return root

def readChunk(file, parent):
    # create new chunk
    chunk = ChunkStruct("", "", 1, 0, "", 0, 0, None, [], 0)
    
    # read chunk header
    chunk.data_type = file.read(4)
    chunk.data_id = file.read(4)
    chunk.version = readLong(file)
    chunk.size = readLong(file)
    len = readLong(file)
    chunk.unknown1 = readLong(file)
    chunk.unknown2 = readLong(file)
    if len > 0:
        chunk.name = readStr(file, len)
        
    chunk.parent = parent
    if parent != None:
        chunk.parent.children.append(chunk)
        
    chunk.offset = file.tell()
    if chunk.data_type == b"FOLD":
        pos = file.tell()
        byteCount = 0
        while byteCount < chunk.size:
            readChunk(file, chunk)
            byteCount = file.tell() - pos
    else:
        file.seek(chunk.size, 1)

def getChunk(parent, path):
    return findChunk(path.split("/"), parent)

def findChunk(ids, parent):
    look_id = ids[0].encode('ASCII')
    found = -1
    for i in range(0, len(parent.children)):
        if parent.children[i].data_id == look_id:
            found = i
            break
    
    if found >= 0:
        if len(ids) == 1:
            return parent.children[found]
        else:
            ids.pop(0)
            return findChunk(ids, parent.children[found])
    else:
        return None

def bytesToWeights(data):
    weights = []
    weights.append(ceil(data[2] / 255.0 * 1000.0 - 0.5) / 1000.0)
    weights.append(ceil(data[1] / 255.0 * 1000.0 - 0.5) / 1000.0)
    weights.append(ceil(data[0] / 255.0 * 1000.0 - 0.5) / 1000.0)
    weights.append(ceil(data[3] / 255.0 * 1000.0 - 0.5) / 1000.0)
    delta = sum(weights) - 1.0
    weights[0] -= delta
    return weights

def unpackVector(data):
    vector = [0,0,0]
    for i in range(1,3):
        sign = get_bit(data[i], 8)
        value = data[i] ^ 8
        if sign == 0:
            value = ((data[i] ^ 0xFF) << 24) >> 24
            value *= -1
        vector[i] = value / 127.0
    return vector

def create_texture(mat, path, type):
    image = None
    for img in bpy.data.images:
        if img.filepath_raw == path:
            image = img
            break
    
    if image == None:
        try:
            image = bpy.data.images.load(path)
        except:
            return

    bsdf = mat.node_tree.nodes["Principled BSDF"]
    tex = mat.node_tree.nodes.new('ShaderNodeTexImage')
    tex.image = image

    mat.node_tree.links.new(bsdf.inputs[type], tex.outputs['Color'])
    

def importMaterials(file, context, mtrl):
    dataInfo = mtrl.children[0]
    file.seek(dataInfo.offset, 0)
    nameLength = readLong(file)
    shaderName = file.read(nameLength).decode('ASCII')
    shaderPath = dataPath + "\\shader\\" + shaderName + ".shader"
    materialName = mtrl.name
    
    # return material if it already exists
    mat = bpy.data.materials.get(materialName)
    if mat:
        return mat
    
    # create new material
    mat = bpy.data.materials.new(materialName)
    mat.use_nodes = True
    
    # read material variables
    variables = {}
    for i in range(1, len(mtrl.children)):
        # get DATAVAR chunk
        dataVar = mtrl.children[i]
        file.seek(dataVar.offset, 0)
        
        # read variable
        varName = readStr(file, readULong(file))
        varType = readLong(file)
        varSize = readLong(file)
        
        # read variable data
        if varType == 0:
            # integer
            variables[varName] = readLong(file)
        elif varType == 1:
            # float
            variables[varName] = readFloat(file)
        elif varType == 3:
            # float2
            variables[varName] = Vector((readFloat(file), readFloat(file)))
        elif varType == 4:
            # float3
            variables[varName] = Vector((readFloat(file), readFloat(file), readFloat(file)))
        elif varType == 5:
            # float4
            variables[varName] = Vector((readFloat(file), readFloat(file), readFloat(file), readFloat(file)))
        elif varType == 8:
            # matrix4x4
            variables[varName] = Matrix([
                    [readFloat(file),readFloat(file),readFloat(file),readFloat(file)],
                    [readFloat(file),readFloat(file),readFloat(file),readFloat(file)],
                    [readFloat(file),readFloat(file),readFloat(file),readFloat(file)],
                    [readFloat(file),readFloat(file),readFloat(file),readFloat(file)]
            ])
        elif varType == 9:
            # texture path
            filePath = readStr(file, varSize)
            variables[varName] = dataPath + "\\" + filePath[:-1] + ".dds"
        elif varType == 10:
            # boolean
            variables[varName] = readByte(file)
    
    # create textures
    if "diffuseTex" in variables:
        create_texture(mat, variables["diffuseTex"], 'Base Color')
    if "normalMap" in variables:
        create_texture(mat, variables["normalMap"], 'Normal')
    if "emissiveTex" in variables:
        create_texture(mat, variables["emissiveTex"], 'Emission')
    if "specularTex" in variables:
        create_texture(mat, variables["specularTex"], 'Specular')
    if "glossTex" in variables:
        create_texture(mat, variables["glossTex"], 'Metallic')
    
    return mat
            

def importBones(file, context, skel, armature):
    # create armature
    if armature == None:
        arma = bpy.data.armatures.new('Armature')
        armature = bpy.data.objects.new('Armature', arma)
        context.collection.objects.link(armature)
        armature.rotation_mode = "QUATERNION"
    
    dataInfo = skel.children[0]
    file.seek(dataInfo.offset, 0)
    numBones = readLong(file)
    
    
    bones = []
    for i in range(numBones):
        dataBone = skel.children[i+1]
        file.seek(dataBone.offset, 0)
        
        # read bone data
        name = dataBone.name
        parent = readLong(file)
        unknown = readLong(file)
        matrix = readMatrix(file)
        transform = matrix.copy()
        if parent >= 0:
            transform = bones[parent].transform @ transform
            bones[parent].numChildren += 1
        
        bones.append(BoneData(name, parent, matrix, transform))
    
        
    # goto edit mode to access edit_bones
    context.view_layer.objects.active = armature
    bpy.ops.object.mode_set(mode='EDIT', toggle=False)
    edit_bones = armature.data.edit_bones
    
    # create bones
    for bone in bones:
        if bone.name in edit_bones:
            continue
        b = edit_bones.new(bone.name)
        if bone.parent >= 0:
            parent = edit_bones[bones[bone.parent].name]
            parent.children.append(b)
            b.parent = parent
            if bones[bone.parent].numChildren == 1:
                edit_bones[bone.parent].tail = b.head
                b.use_connect = True
        b.tail = b.head + Vector((0,0,0.1))
        b.transform(bone.transform)
                
    
    # exit edit mode to save bones so they can be used in pose mode
    bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
    
    return armature
    

def importMesh(file, context, meshName, materials, armature_object):
    # read vertex elements
    numVertexElements = readULong(file)
    vertexElements = [None] * 10
    for i in range(0, numVertexElements):
        typ = readULong(file)
        ver = readULong(file)
        dat = readULong(file)
        element = VertexElement(typ, ver, dat)
        vertexElements[typ] = element
    
    # read vertices
    numVertices = readULong(file)
    vertSize = readULong(file)
    
    vertex_positions = []
    vertex_blendIndices = []
    vertex_blendWeights = []
    vertex_normals = []
    vertex_binormals = []
    vertex_tangents = []
    vertex_colors = []
    vertex_uvs = []
    vertex_uv2 = []
    for i in range(0, numVertices):
        # create vertex
        dataLeft = vertSize
        
        # read position
        if vertexElements[0] != None:
            x = readFloat(file)
            y = readFloat(file)
            z = readFloat(file)
            vertex_positions.append((x,y,z))
            dataLeft -= 12
        
        # read blend indices
        if vertexElements[1] != None:
            blendIndices = [readUByte(file), readUByte(file), readUByte(file), readUByte(file)]
            dataLeft -= 4
            vertex_blendIndices.append(blendIndices)
        
        # read blend weights
        if vertexElements[2] != None:
            blendWeights = [readUByte(file), readUByte(file), readUByte(file), readUByte(file)]
            dataLeft -= 4
            blendWeights = bytesToWeights(blendWeights) # 4 bytes
            vertex_blendWeights.append(blendWeights)
        
        # read vertex normal
        if vertexElements[3] != None:
            if vertexElements[3].data_type == 2:
                data = [];
                for j in range(0,4):
                    data.append(readByte(file))
                    dataLeft -= 1
                vector = unpackVector(data)
                normal = (vector[0], vector[1], vector[2]) # 4 bytes
            elif vertexElements[3].data_type == 4:
                normal = (readFloat(file), readFloat(file), readFloat(file))
                dataLeft -= 12
            vertex_normals.append(normal)
        
        # read vertex binormal 
        if vertexElements[4] != None:
            if vertexElements[4].data_type == 2:
                binormal = readLong(file) # 4 bytes
                dataLeft -= 4
            elif vertexElements[4].data_type == 4:
                binormal = (readFloat(file), readFloat(file), readFloat(file))
                dataLeft -= 12
            vertex_binormals.append(binormal)
        
        # read vertex tangents
        if vertexElements[5] != None:
            if vertexElements[5].data_type == 2:
                tangent = readLong(file) # 4 bytes
                dataLeft -= 4
            elif vertexElements[5].data_type == 4:
                tangent = (readFloat(file), readFloat(file), readFloat(file))
                dataLeft -= 12
            vertex_tangents.append(tangent)
        
        # read vertex color
        if vertexElements[6] != None:
            color = Color(readByte(file), readByte(file), readByte(file), readByte(file))
            vertex_colors.append(color)
            dateLeft -= 4
        
        # read texture coordinates
        if vertexElements[8] != None:
            x = readFloat(file)
            y = -readFloat(file)
            vec = Vector((x, y))
            vertex_uvs.append(vec)
            dataLeft -= 8
        
        if vertexElements[9] != None:
            vertex_uv2.append( Vector((readFloat(file), 1.0-readFloat(file))))
            dataLeft -= 8
        
        if dataLeft > 0:
            console_print("WARNING! did not read vertex data till the end. skipped: ", dataLeft)
            file.seek(dataLeft, 1)
        elif dataLeft < 0:
            console_print("WARNING! overread vertex data by", -dataLeft)
    
    
    # read faces
    file.seek(8, 1)
    vertPerFace = readULong(file)
    numFaces = int(readULong(file) / vertPerFace)
    faces = []
    face_uvs = []
    for i in range(0, numFaces):
        triangle = [readUShort(file), readUShort(file), readUShort(file)]
        faces.append(triangle)
        face_uvs.append((vertex_uvs[triangle[0]], vertex_uvs[triangle[1]], vertex_uvs[triangle[2]]))
    
    # read material name
    material_name = readStr(file, readLong(file))
    
    # read skin
    numSkinBones = readULong(file)
    skinBones = []
    blendMatrix = []
    for i in range(0, numSkinBones):
        # read matrices
        worldMatrix = readMatrix(file)
        inverseWorldMatrix = readMatrix(file)
        
        # read bone name
        skinBoneName = readStr(file, readULong(file))
        skinBones.append(skinBoneName)
    
    # read unknowns
    unknown1 = readULong(file)
    unknown2 = readULong(file)

    # create mesh
    mesh = bpy.data.meshes.new(meshName)
    bm = bmesh.new()
    
    # add vertices
    for i in range(numVertices):
        v = bm.verts.new(vertex_positions[i])
        v.normal = vertex_normals[i]
    
    bm.verts.ensure_lookup_table()
    
    # add faces
    for f_idx in faces:
        try:
            bm.faces.new([bm.verts[i] for i in f_idx])
        except:
            pass
    
    # add uv
    uv_layer = bm.loops.layers.uv.verify()
    for face_id, face in enumerate(bm.faces):
        for loop_id, loop in enumerate(face.loops):
            loop_uv = loop[uv_layer]
            loop_uv.uv = face_uvs[face_id][loop_id]
    
    # add normals
    for i in range(len(bm.verts)):
        bm.verts[i].normal = vertex_normals[i]
    
    # finalize mesh
    bm.to_mesh(mesh)
    mesh.update()

    mesh_object = bpy.data.objects.new(meshName, mesh)
    mesh_object.rotation_mode = "QUATERNION"
    
    # add vertex weights
    vertex_groups = {}
    for vertexIndex in range(0, numVertices):
        for idx, blendIndex in enumerate(vertex_blendIndices[vertexIndex]):
            if blendIndex < 0:
                continue
            
            bone = skinBones[blendIndex]
            if not bone in vertex_groups:
                vertex_groups[bone] = mesh_object.vertex_groups.new(name=bone)
                
            vertex_groups[bone].add([vertexIndex], vertex_blendWeights[vertexIndex][idx], 'ADD')
    
    # add armature modifier
    arm_modifier = mesh_object.modifiers.new('Armature', type='ARMATURE')
    arm_modifier.object = armature_object
        
    
    # add material
    for mat in materials:
        if mat.name.startswith(material_name):
            mesh_object.data.materials.append(mat)
            break
    else:        
        console_print("material not found: ", material_name)
    
    mesh_object.parent = armature_object
    context.collection.objects.link(mesh_object)
    

class ImportRelicData(Operator, ImportHelper):
    """This appears in the tooltip of the operator and in the generated docs"""
    bl_idname = "relic_import.relic_model_data"  # important since its how bpy.ops.import_test.some_data is constructed
    bl_label = "Relic Model Import"
    bl_options = {'UNDO'}

    # ImportHelper mixin class uses this
    filename_ext = ".model"

    filter_glob: StringProperty(
        default="*.model",
        options={'HIDDEN'},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )
    
    files: CollectionProperty(
            type=bpy.types.OperatorFileListElement,
            options={'HIDDEN', 'SKIP_SAVE'},
    )

    # List of operator properties, the attributes will be assigned
    # to the class instance from the operator settings before calling.
    merge_bones: BoolProperty (
        name="Merge Bones",
        description="Merges the bones of different models",
        default=True
    )

    def execute(self, context):
        console_print("running relic model import.")
       
        
        armature = None
        if self.merge_bones:
            for obj in bpy.context.selected_objects:
                if obj.type == "ARMATURE":
                    armature = obj
        
        folder = (os.path.dirname(self.filepath))
        
        for modelfile in self.files:
            path_to_file = (os.path.join(folder, modelfile.name))
            
            f = open(path_to_file, 'rb')

            materials = []
            
            chunkroot = parseChunkyFile(f)
            
            # import stuff
            modl = getChunk(chunkroot, "MODL")
            if modl != None:
                for i in range(0, len(modl.children)):
                    if modl.children[i].data_type == b"FOLD" and modl.children[i].data_id == b"MTRL":
                        mat = importMaterials(f, context, modl.children[i])
                        materials.append(mat)
            else:
                console_print("no materials found")
                
            skel = getChunk(chunkroot, "MODL/SKEL")
            if skel != None:             
                armature = importBones(f, context, skel, armature)
            else:
                console_print("no bones found")
                
            mgrp = getChunk(chunkroot, "MODL/MESH/MGRP")
            if mgrp != None and mgrp.size > 0:
                for i in range(0, len(mgrp.children)):
                    if mgrp.children[i].data_type == b"FOLD" and mgrp.children[i].data_id == b"MESH":
                        imdg = getChunk(mgrp.children[i], "IMDG")
                        for j in range(0, len(imdg.children)):
                            if imdg.children[j].data_type == b"FOLD" and imdg.children[j].data_id == b"MESH":
                                imod = getChunk(imdg.children[j], "IMOD")
                                for k in range(0, len(imod.children)):
                                    if imod.children[k].data_type == b"FOLD" and imod.children[k].data_id == b"MESH":
                                        data = getChunk(imod.children[k], "TRIM/DATA")
                                        f.seek(data.offset, 0)
                                        importMesh(f, context, imod.children[k].name, materials, armature)
                                        
            else:
                console_print("no meshes found")
                  
            f.close()
            
            bpy.ops.object.select_all(action='DESELECT')
            armature.select_set(True)

        return {'FINISHED'}


# Only needed if you want to add into a dynamic menu
def menu_func_import(self, context):
    self.layout.operator(ImportRelicData.bl_idname, text="Relic Model Import")


def register():
    bpy.utils.register_class(ImportRelicData)
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)


def unregister():
    bpy.utils.unregister_class(ImportRelicData)
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)


if __name__ == "__main__":
    register()